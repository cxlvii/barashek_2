module.exports = {
  options: {
    browsers: ['last 5 version']
  },
  multiple_files: {
    expand: true,
    flatten: true,
    src: 'build/css/style.css',
    dest: 'build/css/'
  }
}
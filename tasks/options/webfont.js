module.exports = {
  icons:{
    src: "src/icons/*.svg",
    dest: 'src/fonts/icons',
    destCss: 'src/scss/icons',
    options: {
        font: 'Icons-Regular',
        htmlDemo: true,
        hashes: true,
        relativeFontPath: '/css/fonts/icons/',
        templateOptions: {
            stylesheet: 'scss',
            baseClass: 'icon',
            classPrefix: 'icon_'
        }
    }
  }
};
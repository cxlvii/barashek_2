module.exports = {
  dist: {
    src: [
      'src/app/plugins/jstorage.min.js',
      'src/app/plugins/angular.min.js',
      'src/app/plugins/angular-route.js',
      'src/app/plugins/angular-modal-service.min.js',
      'src/app/plugins/mask.js',
      'src/app/plugins/angular-md5.js',
      'src/app/plugins/barashek-socialLogin.js',
      'src/app/plugins/likely.js',

      'src/app/barashekApp.js',
      'src/app/routing.js',

      'src/app/services/*.js',
      'src/app/controllers/*.js',
      'src/app/directives/*.js',

      'src/app/filters/*.js'
    ],
    dest: 'build/js/app.js'
  }
}
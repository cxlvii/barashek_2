module.exports = {
  dist: { // Target
    files: [{
        expand: true,
        cwd: 'src/slim',
        src: ['{,*/}*.slim'],
        dest: 'build/',
        ext: '.html'
      }]
  },
  // dev: { // Another target
  //   options: { // Target options
  //     pretty: true
  //   },
  //   files: {
  //     'index.html': 'index.slim',
  //     'page.html': [
  //       'header.html',
  //       'body.html',
  //       'footer.html' // Maybe you need one extra file in dev
  //     ]
  //   }
  // }
}

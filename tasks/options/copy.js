module.exports = {
  main: {
    expand: true,
    cwd: 'src/fonts/',
    src: ['**'],
    dest: 'build/css/fonts'
  },
  fakeBackend: {
    expand: true,
    cwd: 'src/fakeBackend/',
    src: ['**'],
    dest: 'build/fakeBackend'
  },
  images: {
    expand: true,
    cwd: 'src/i/',
    src: ['**'],
    dest: 'build/i'
  }
}
module.exports = {
  build: {
    options: {
      mangle: false
    },
    src: 'build/js/app.js',
    dest: 'build/js/app.min.js'
  }
}
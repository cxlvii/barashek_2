Barashek.directive('stickyHeader', ['$window', '$document', function($window, $document) {

    var $sticky = {},
        body = angular.element($document[0].body),

    scroll = function scroll() {
      // console.log($sticky);
      var headerHeight = $sticky.clientHeight;
      if(headerHeight < $window.pageYOffset){
        if( !angular.element($sticky).hasClass("is-sticked")){
          body.css('padding-top', headerHeight+'px' );
          angular.element($sticky).addClass("is-sticked");
        }
      }else{
        body.css('padding-top', 0);
        angular.element($sticky).removeClass("is-sticked");
      }
    },

    link = function($scope, element, attrs, ctrl, transclude) {
      transclude($scope, function(clone) {
        element.append(clone);
      });
      $sticky = element[0];
    };

    angular.element($window).off('scroll', scroll).on('scroll', scroll);

    return {
      restrict: 'EA',
      transclude: true,
      link: link
    };
  }]);
Barashek.directive("cardDir", function($rootScope, ORDER_EVENTS ){
  return{
    restrict: 'EA',
    link:  function(scope, element, attrs) {

      scope.addToOrder = function(product, $event){
        $rootScope.$broadcast(ORDER_EVENTS.itemAdded, {product: product});

        if(typeof TweenMax !== 'undefined'){
          var isMobile = (typeof window.orientation !== 'undefined');
          // fly away animation
          var productImage = angular.element(element[0].querySelector('.card_media-img'));
          var basket = angular.element(document.querySelector('.basket_content-count'));
          var start = scope.getElementOffset(productImage[0]);
          var dest = scope.getElementOffset(basket[0]);
          var timing = (start.top - dest.top) / 800;

          // a bit slower animation for mobile devices
          timing = (isMobile) ? timing * 2 : timing;

          var amountArray = [];

          for(var i = 0; i < product.amount; i++){
            var newImage = angular.element('<img src="'+productImage.attr('src')+'" alt="">');

            angular.element(document.body).prepend(newImage);
            TweenMax.set(newImage, {
              position: "absolute",
              width: start.width,
              height: start.height,
              left: start.left,
              top: start.top,
              zIndex: 1000
            });

            amountArray.push(newImage);
          }

          TweenMax.staggerTo(amountArray, timing,{
            top: dest.top + dest.height/2,
            left:dest.left + dest.width/2,
            width: '10px',
            height: '10px'
            },
            .2, // stagger timing
            function(){ // oncomplete function
              angular.forEach(amountArray, function(elem, index){
                elem.remove();
              });
            }
          );
          TweenMax.fromTo(productImage, .5,{
            scale: .2,
            alpha: 0
          },{
            delay: (amountArray.length > 1 ) ? (.2 * (amountArray.length-1)) : 0,
            scale: 1,
            alpha: 1
          });
        }


      };

      scope.changeAmount = function(operator, product){
        var newVal = parseInt(product.amount + parseInt(operator+1));
        product.amount = (newVal >= 1 && newVal <= 99) ? newVal : product.amount;
      };

      scope.checkInput = function(product){
        product.amount = (isNaN(parseInt(product.amount))) ? 1 : product.amount;
        product.amount = (parseInt(product.amount) <= 0) ? 1 : parseInt(product.amount);
      };

    }
  }
});
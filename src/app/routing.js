Barashek.config(function ($routeProvider, $locationProvider ) {

  $locationProvider.html5Mode(true);

  $routeProvider.when('/', {
    templateUrl: "views/home.html"
  }).when('/basket', {
    templateUrl: "views/basket.html"
  }).when('/order', {
    templateUrl: "views/order.html"
  }).when('/order_complete', {
    templateUrl: "views/ty.html"
  }).otherwise({
    redirectTo: '/'
  });
});
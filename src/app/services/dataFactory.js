Barashek.factory('dataFactory', ['$http', function($http) {
  return dataFactory = {
    getProducts: function(){
      return $http.get('fakeBackend/products.json');
    },
    sendOrder: function(order){
      return $http.get('fakeBackend/ok.json', order );
    },
    login: function(credentials){
      return $http.post('fakeBackend/login.json', credentials );
    },
    reconnectGoogle: function(params){
      return $http.get('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='+params );
    },
    connectOK: function(params){
      return $http({url: 'https://api.ok.ru/fb.do', method: 'get', params: params});
    }
  };
}]);
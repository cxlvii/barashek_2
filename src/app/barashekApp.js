var Barashek = angular.module("barashekApp", ['ngRoute', 'angularModalService', 'socialLogin', 'ui.mask' ]);

Barashek.constant('AUTH_TYPES', {
    google : 'google',
    vk : 'vk',
    ok : 'ok',
    native : 'native'
}).constant('AUTH_EVENTS', {
    loginSuccess : 'auth-login-success',
    loginFailed : 'auth-login-failed',
    logoutSuccess : 'auth-logout-success',
    sessionTimeout : 'auth-session-timeout',
    notAuthenticated : 'auth-not-authenticated',
    notAuthorized : 'auth-not-authorized'
}).constant('ORDER_EVENTS', {
    itemAdded : 'item-added',
    itemRemoved : 'item-removed'
}).constant('UI_EVENTS', {
    modalOpened : 'modal-added',
    modalClosed : 'modal-closed'
});

// Barashek.config(function ($httpProvider) {
//   $httpProvider.interceptors.push([
//     '$injector',
//     function ($injector) {
//       return $injector.get('AuthInterceptor');
//     }
//   ]);
// });

Barashek.config(['$compileProvider', function ($compileProvider) {
  // disable debug info
  // $compileProvider.debugInfoEnabled(false);
}]);
// Barashek.config(function(socialProvider){
//     socialProvider.setGoogleKey("729094022965-fm6ka52gvj9c6qn56h22q3rthklbeu63.apps.googleusercontent.com");
// });

Barashek.config(['uiMask.ConfigProvider', function(uiMaskConfigProvider) {
  uiMaskConfigProvider.maskDefinitions({
    'a': /[a-z]/,
    'A': /[A-Z]/,
    'cyr': /[а-яА-ЯЁё]/,
    '*': /[a-zA-Zа-яА-ЯЁё0-9]/,
  });
  uiMaskConfigProvider.clearOnBlur(false);
  uiMaskConfigProvider.clearOnBlurPlaceholder(true);
  uiMaskConfigProvider.eventsToHandle(['input', 'keyup', 'click']);
}]);

// Barashek.run(function($rootScope, loginService) {
  //before each state change, check if the user is logged in
  // console.log('run');
  // $rootScope.$on('$routeChangeStart', function(event){
    // check user
    // console.log(loginService.getProvider());
    // console.log(next);
    // preloader
    // $rootScope.globPreload = true;

  // });

  // $transitions.onFinish({}, function($transition$){

    // preloader
    // $rootScope.globPreload = false;

  // });

  /* To show current active state on menu */
  // $rootScope.getClass = function(path) {
  //   if ($state.current.name == path) {
  //     return "active";
  //   } else {
  //     return "";
  //   }
  // };

//   $rootScope.logout = function(){
//     Auth.logout();
//   };

// });

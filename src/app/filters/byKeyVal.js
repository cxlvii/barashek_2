Barashek.filter('byKeyVal', function() {
  return function(items, field, val) {
    var filtered = [];
    angular.forEach(items, function(item) {
      if(item[field] === val){
        filtered.push(item);
      }
    });
    return filtered;
  };
});
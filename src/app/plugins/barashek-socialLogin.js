/*
  Writen for Barashek
  Uses some constants from Barashek project
  Uses dataFactory factory from Barashek
  Depends on angular-md5 lib
  Depends on jStorage lib
  Uses some dirty hack for Odnoklassniki oAuth
*/
var SocialLogin = angular.module('socialLogin', ['angular-md5']);
SocialLogin.constant('SETTINGS',{
  google:{
    url: 'https://apis.google.com/js/api:client.js',
    client_id: '729094022965-fm6ka52gvj9c6qn56h22q3rthklbeu63.apps.googleusercontent.com',
    cookiepolicy: 'single_host_origin',
    scope: 'profile',
    access_type: 'offline'
  },
  vk:{
    url: '//vk.com/js/api/openapi.js',
    apiId: '5623100'
  },
  ok:{
    url: 'https://connect.ok.ru/oauth/authorize?',
    client_id: '1248183808',
    application_key: 'CBAHMGGLEBABABABA'
  }
});

SocialLogin.run(function(SETTINGS, loginService){
  var loaded = { g: false, vk: false},
      isLoaded = function(){
        return loaded.g && loaded.vk;
      },
      appendScript = function(_src, _onload){
        var d = document, gJs, ref;
        ref = d.getElementsByTagName('script')[0]
        gJs = d.createElement('script');
        gJs.async = true;
        gJs.defer = true;
        gJs.src = _src;
        gJs.onload = _onload;
        ref.parentNode.insertBefore(gJs, ref);
      };
  // set Google
  appendScript(SETTINGS.google.url, function() {
    gapi.load('auth2', function() {
      gapi.auth2.init(SETTINGS.google);
      loaded.g = true;
      if(isLoaded && loginService.getProvider() === 'google'){ loginService.autoAuth('google');}
    });
  });

  // set VK
  appendScript(SETTINGS.vk.url, function() {
    VK.init(SETTINGS.vk);
    loaded.vk = true;
    if(isLoaded && loginService.getProvider() === 'vk'){ loginService.autoAuth('vk'); }
  });

  // set OK
  // Odnoklassniki works different than other (normal) APIs
  if(loginService.getProvider() === 'ok'){
    // window.close();
    loginService.autoAuth('ok');
  }
});


SocialLogin.directive("socialLogin", function($rootScope, loginService){
  return {
    restrict: 'EA',
    scope: {},
    replace: true,
    link: function(scope, ele, attr){
      ele.on('click',function(){
        if(typeof attr['socialLogin'] !== 'undefined'){
          loginService.login(attr['socialLogin']);
        }
      });
    }
  }
});

SocialLogin.factory("loginService", function($window, $rootScope, $http, md5, dataFactory, jStorage, $httpParamSerializer, AUTH_EVENTS, AUTH_TYPES, SETTINGS) {
  return {
    setProvider: function(provider) {
      jStorage.set('_login_provider', provider);
    },
    getProvider: function() {
      return jStorage.get('_login_provider');
    },
    logout: function() {
      var provider = jStorage.get('_login_provider');
      switch (provider) {
        case "google":
          var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function () {
            jStorage.deleteKey('_gat');
          });
        break;
        case "vk":
          VK.Auth.logout(function () {
            jStorage.deleteKey('_vat');
          });
        break;
        case "ok":
          jStorage.deleteKey('_oat');
          jStorage.deleteKey('_oss');
        break;
      }
      // console.log( 'User signed out from ' + provider );
    },
    login: function(provider){
      switch(provider){
        case "vk":
          VK.Auth.login(function(response) {
            if (typeof response.session !== 'undefined') {
              var user = {
                    name: response.session.user.first_name + ' ' + response.session.user.last_name
                  };
              $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, {user: user, authType: AUTH_TYPES.vk});
              jStorage.set('_vat', response.session.sid, {TTL: 7200});
            }
          }, (4194304+65536));
        break;
        case "google":
            var gauth = gapi.auth2.getAuthInstance();
            gauth.signIn().then(function(googleUser) {
              var profile = googleUser.getBasicProfile(),
                  user = {
                    name: profile.getName(),
                    email: profile.getEmail()
                  };
              var response = googleUser.getAuthResponse();
              $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, {user: user, authType: AUTH_TYPES.google});
              jStorage.set('_gat', response.id_token, {TTL: response.expires_in * 1000});
            });
        break;
        case "ok":
          /* костыли */
          var urlParams = {
            client_id: SETTINGS.ok.client_id,
            scope: 'VALUABLE_ACCESS',
            redirect_uri: $window.location.href + 'ok-auth.html',
            response_type: 'token'
          };
          var authWindow = $window.open(SETTINGS.ok.url + '' + $httpParamSerializer(urlParams),
                                        'Авторизация',
                                        'height=400,width=750,scrollbars,status,modal=yes,alwaysRaised=yes');

          /*
            одноклассники возвращают данные сессии в виде параметров урла
            в тот попап в котором был открыт диалог авторизации (!sic)
            поэтому существует отдельная страница которая ловит этот урл, парсит
            и публикует токен и секретный ключ в сторедж

            Тут вешается слушатель jStorage, который ждет когда данные будут опубликованы,
            После - закрывается попап, токен и секретный ключ пишутся в сторедж
            переменные в window обнуляются

              в попапе происходит следующее:
              var query = location.hash.substr(1);
              var result = {};
              query.split("&").forEach(function(part) {
                var item = part.split("=");
                result[item[0]] = decodeURIComponent(item[1]);
              });
              window.opener.okAtoken = result.access_token;
              window.opener.sskey = result.session_secret_key;


          */
          jStorage.subscribe("ch1", function(channel, channelData){
            if(angular.isDefined(channelData.session_secret_key && channelData.access_token && channelData.expires_in)){
              authWindow.close();
              dataFactory.connectOK({
                method: 'users.getCurrentUser',
                access_token: channelData.access_token,
                format: 'json',
                sig: md5.createHash('application_key='+SETTINGS.ok.application_key+'format=jsonmethod=users.getCurrentUser'+channelData.session_secret_key),
                application_key: SETTINGS.ok.application_key
              }).then(function(response){
                if(typeof response.data.name !== 'undefined'){
                  var user = {
                    name: response.data.name
                  };
                  $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, {user: user, authType: AUTH_TYPES.ok});
                  jStorage.set('_oat', channelData.access_token, {TTL: channelData.expires_in * 1000});
                  jStorage.set('_oss', channelData.session_secret_key, {TTL: channelData.expires_in * 1000});
                }
              });
            }else{
              console.log('Problems with jStorage payload');
            }
           });

        break;
      }
    },


    autoAuth: function(provider){
      if(typeof provider !== 'undefined'){
        switch(provider){
          case 'google':
            var idt = jStorage.get('_gat');
            if(angular.isString(idt)){
              dataFactory.reconnectGoogle(idt).then(function(response){
                if(response.status === 200){
                  var user = {
                    name: response.data.name,
                    email: response.data.email
                  };
                  $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, {user: user, authType: AUTH_TYPES.google});
                }
              });
            }
          break;
          case 'vk':
            var idt = jStorage.get('_vat');
            if(angular.isString(idt)){
              // console.log(idt);
              VK.Api.call('users.get', {access_token: idt},function(r){
                // console.log(r.response[0]);
                if(angular.isDefined(r.response[0])){
                  var user = {
                      name: r.response[0].first_name + ' ' + r.response[0].last_name
                    };
                  $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, {user: user, authType: AUTH_TYPES.vk});
                }
              });
            }
          break;
          case 'ok':
            var oss = jStorage.get('_oss'),
                oat = jStorage.get('_oat');
            if(angular.isString(oat) && angular.isString(oss)){
              dataFactory.connectOK({
                method: 'users.getCurrentUser',
                access_token: oat,
                format: 'json',
                sig: md5.createHash('application_key='+SETTINGS.ok.application_key+'format=jsonmethod=users.getCurrentUser'+oss),
                application_key: SETTINGS.ok.application_key
              }).then(function(response){
                if(typeof response.data.name !== 'undefined'){
                  var user = {
                    name: response.data.name
                  };
                  $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, {user: user, authType: AUTH_TYPES.ok});
                }
              });
            }
          break;
        }
        // dataFactory
      }else{
        console.log('no oauth detected');
        return false;
      }
    }
  }
});
Barashek.controller("globalCtrl", function($scope, $rootScope, $timeout, $location, ModalService, loginService, dataFactory, $location, $filter, AUTH_EVENTS, AUTH_TYPES, ORDER_EVENTS, UI_EVENTS){

  $scope.products = {};
  $scope.slider = {};
  $scope.basket = [];
  $scope.orderSum = 0;
  $scope.state = {
    productsReceived: false,
    isLoading: true,
    sideMenuOpened: false,
    modalOpened: false,
    sliderReady: false,
    userLogedIn: false,
    userData: {
      name: null,
      email: null,
      phone: null,
      authType: null
    }
  };
  // separate object to store user data for forms
  $scope.userdata = {};

  var setCurrentUser = function(e, data){
    if(typeof data.user !== 'undefined' && typeof data.authType !== 'undefined'){
      $scope.state.userData.name = data.user.name;
      $scope.state.userData.email = data.user.email;
      $scope.state.userData.authType = data.authType;
      angular.copy($scope.state.userData, $scope.userdata);
      loginService.setProvider($scope.state.userData.authType);
      if($scope.state.modalOpened) { $scope.closeLoginModal(); }
      $timeout(function(){
        $scope.state.userLogedIn = true;
      },100);
      // console.log($scope.state.userLogedIn);
    }else{
      console.log("can't set current user (data params error)");
    }
  };

  $scope.logout = function(e, data){
    loginService.logout();
    angular.forEach($scope.state.userData, function(element, i){
      $scope.state.userData[i] = null;
    });
    angular.forEach($scope.userdata, function(element, i){
      $scope.userdata[i] = null;
    });
    $scope.state.userLogedIn = false;
  };

  var addItemInBasket = function(e, args){
    var canBeMerged = false;
    angular.forEach($scope.basket, function(element, i){
      if(element.id === args.product.id){
        canBeMerged = true;
        element.amount = element.amount + args.product.amount;
      }
    });
    if(!canBeMerged){
      $scope.basket.push({
        id: args.product.id,
        img: args.product.img,
        title: args.product.title,
        text: args.product.text,
        amount: args.product.amount,
        price: args.product.price
      });
    }
  };

  var removeItemFromBasket = function(e, args){
    var index = $scope.basket.indexOf(args.product);
    $scope.basket.splice(index, 1);
  };


  $scope.closeLoginModal = function() {
    ModalService.hideModal();
    $rootScope.$broadcast(UI_EVENTS.modalClosed);
  };

  $scope.showLoginModal = function(tab) {
    ModalService.showModal({
      templateUrl: "templates/login-modal.html",
      controller: "LoginController",
      inputs: {
        tabName: tab
      }
    }).then(function(modal) {
      modal.close.then(function(result) {
        // $scope.customResult = "All good!";
      });
    });
  };


  $scope.currentUser = null;
  // $scope.userRoles = USER_ROLES;
  // $scope.isAuthorized = Auth.isAuthorized;

  // $rootScope.$on(AUTH_EVENTS.notAuthorized, showNotAuthorized);
  // $rootScope.$on(AUTH_EVENTS.notAuthenticated, showLoginDialog);
  // $rootScope.$on(AUTH_EVENTS.sessionTimeout, showLoginDialog);
  // $rootScope.$on(AUTH_EVENTS.logoutSuccess, clearCurrentUser);
  $rootScope.$on(AUTH_EVENTS.loginSuccess, setCurrentUser);


  $rootScope.$on(ORDER_EVENTS.itemAdded, addItemInBasket);
  $rootScope.$on(ORDER_EVENTS.itemRemoved, removeItemFromBasket);

  $rootScope.$on(UI_EVENTS.modalOpened, function(){
    $scope.state.modalOpened = true;
  });
  $rootScope.$on(UI_EVENTS.modalClosed, function(){
    $scope.state.modalOpened = false;
  });




  $scope.getSliderItems = function(slider) {
    var sliderCollection = [];

    angular.forEach(slider, function(value, key) {
      var product = $filter('byKeyVal')($scope.products, 'id', slider[key].id);
      product[0].sliderImg = slider[key].sliderImg;
      sliderCollection.push(product[0]);
    });
    if(sliderCollection.length > 0){
      $scope.state.sliderReady = true;
    }
    return sliderCollection;
  };

  $scope.getElementOffset = function(element) {
    var de = document.documentElement;
    var box = element.getBoundingClientRect();
    var top = box.top + window.pageYOffset - de.clientTop;
    var left = box.left + window.pageXOffset - de.clientLeft;
    return { top: top, left: left, width: box.width, height: box.height };
  };

  function init(){
    // check auth
    // dataFactory.reconnectGoogle
    // loginService.autoAuth();
    // loginService.login(loginService.getProvider());
    // get products and slider
    dataFactory.getProducts().then(function (response) {
      angular.forEach(response.data.products, function(element,i){
        element.amount = 1;
      });
      $scope.products = response.data.products;
      $scope.slider = $scope.getSliderItems(response.data.slider);
      $scope.state.productsReceived = true;
      $scope.state.isLoading = false;
    }, function (error) {
      console.log('Unable to load products data: ' + error.message);
    });
  }

  init();

});
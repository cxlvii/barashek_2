Barashek.controller("basketCtrl", function($scope, $rootScope, $location, dataFactory, ORDER_EVENTS){

  // $scope.orderSum = 0;
  $scope.totalCount = 0;
  $scope.xTimes = 'is-x1';
  // $scope.orderData = {};



  var updateBasket = function(){
    var totalSum = 0,
        totalCount = 0;
    angular.forEach($scope.basket, function(element, i){

      totalSum += parseInt(element.price * element.amount);
      // get portions count
      totalCount += parseInt(element.amount);
    });
    $scope.$parent.orderSum = totalSum;
    $scope.totalCount = totalCount;
    // css size multiplier class
    $scope.xTimes = 'is-x' + (($scope.totalCount >= 5) ? '5' : $scope.totalCount);
  };

  $scope.modifyOrderCount = function(operator, product){
    var newVal = parseInt(product.amount + parseInt(operator+1));
    product.amount = (newVal >= 1 && newVal <= 99) ? newVal : product.amount;
  };

  $scope.removePosition = function(product){
    $rootScope.$broadcast(ORDER_EVENTS.itemRemoved, {product: product});
  };


  $scope.orderForm = function(){
    $location.path('order');
  };



  $scope.$watch('basket', updateBasket, true);

});
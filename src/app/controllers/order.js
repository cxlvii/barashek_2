Barashek.controller("orderCtrl", function($scope, $location, dataFactory, ORDER_EVENTS){


  $scope.prepareOrder = function(order){
    order.products = [];
    angular.forEach($scope.basket, function(element,i){
      order.products.push({
        id: element.id,
        amount: element.amount
      });
    });
    dataFactory.sendOrder(order).then(function (response) {
      console.log('order sended: ',order);
      $location.path('order_complete');
    }, function (error) {
      console.log('Unable to send order: ' + error.message);
    });
  };

});
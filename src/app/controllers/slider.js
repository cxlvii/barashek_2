Barashek.controller('sliderCtrl', function($scope, $rootScope, ORDER_EVENTS) {

  $scope.addToOrder = function(slide, evt){
    $rootScope.$broadcast(ORDER_EVENTS.itemAdded, {product: slide});
  };

});